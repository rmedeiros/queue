queue: main.o queue.o
	gcc -g main.o queue.o -o queue

queue.o: queue.h queue.c
	gcc -c queue.c

main.o: main.c
	gcc -c main.c

clear:
	rm queue
	rm *.o
