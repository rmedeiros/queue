#ifndef QUEUE_H_
#define QUEUE_H_

/* structure defining a list node */

typedef struct queue{
	int value;
	struct queue *next;
} Queue;

/* prototype of insert queue function */
Queue *insert(int val, Queue *q);

#endif
